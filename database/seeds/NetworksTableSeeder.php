<?php

use Illuminate\Database\Seeder;

class NetworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('networks')->insert([
            [
                'location_id' => 1, 
                'name' => 'LAN 1',
                'service_provider' => 'PLDT',
                'status_id' => 5,
                'remarks' => 'None'
            ]
        ]);
    }
}
