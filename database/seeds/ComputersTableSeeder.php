<?php

use Illuminate\Database\Seeder;

class ComputersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('computers')->insert([
            ['location_id' => 1, 'pc_number' => 1],
            ['location_id' => 2, 'pc_number' => 2]
        ]);
    }
}
