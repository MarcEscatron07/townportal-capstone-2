<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisposalDetail extends Model
{
    public function disposalArchive() {
        return $this->belongsTo(DisposalArchive::class);
    }
}
