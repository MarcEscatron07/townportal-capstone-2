<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    public function location() {
    	return $this->belongsTo(Location::class);
    }

    public function status() {
    	return $this->belongsTo(Status::class);
    }
}
