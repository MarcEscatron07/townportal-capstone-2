<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function peripheral() {
    	return $this->hasMany(Peripheral::class);
    }

    public function software() {
    	return $this->hasMany(Software::class);
    }

    public function utility() {
    	return $this->hasMany(Utility::class);
    }
}
