<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Computer;
use App\Desktop;
use App\Peripheral;
use App\MaintenanceLog;
use App\DisposalArchive;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $mcount = MaintenanceLog::all();
        session(['maintenancecount' => count($mcount)]);

        $dcount = DisposalArchive::all();
        session(['disposalcount' => count($dcount)]);
        
        $computerCount = count(Computer::all());
        $desktopCount = count(Desktop::all());
        $peripheralCount = count(Peripheral::all());

        return view('home', compact('computerCount','desktopCount','peripheralCount'));
    }
}
