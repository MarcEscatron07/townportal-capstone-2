<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Desktop extends Model
{
	use SoftDeletes;
	
    public function computer() {
    	return $this->belongsTo(Computer::class);
    }

    public function status() {
    	return $this->belongsTo(Status::class);
    }
}
