<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisposalArchive extends Model
{
    public function disposalDetail() {
        return $this->hasOne(DisposalDetail::class);
    }
}
