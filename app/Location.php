<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function computer() {
    	return $this->hasMany(Computer::class);
    }

    public function network() {
    	return $this->hasMany(Network::class);
    }
}
