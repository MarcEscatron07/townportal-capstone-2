<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function() {
	// place routes here when content needs to be accesssed only when authenticated
	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/profile', 'ProfileController@index');
	Route::patch('/profile/image/{userID}', 'ProfileController@updateImage');
	Route::patch('/profile/user/{userID}', 'ProfileController@updateUser');
	Route::patch('/profile/changepassword/{userID}', 'ProfileController@updatePassword');
	Route::delete('/profile/deleteaccount/{userID}', 'ProfileController@deleteAccount');

	// Route::get('/computers/create', 'ComputerController@create');
	// Route::post('/computers', 'ComputerController@store');
	// Route::get('/computers', 'ComputerController@index');
	// Route::get('/computers/{computerId}/edit', 'ComputerController@edit');
	// Route::patch('/computers/{computerId}', 'ComputerController@update');
	// Route::delete('/computers/{computerId}', 'ComputerController@destroy');

	Route::resource('/computers', 'ComputerController');

	Route::delete('/desktops/delete/all/{userID}', 'DesktopController@deleteAll');
	Route::resource('/desktops', 'DesktopController');
	Route::patch('/desktops/status/{statusID}/desktop/{desktopID}', 'DesktopController@updateStatus');

	Route::delete('/peripherals/delete/all/{userID}', 'PeripheralController@deleteAll');
	Route::get('/peripherals/monitor', 'PeripheralController@showMonitor');
	Route::get('/peripherals/keyboard', 'PeripheralController@showKeyboard');
	Route::get('/peripherals/mouse', 'PeripheralController@showMouse');
	Route::get('/peripherals/headset', 'PeripheralController@showHeadset');
	Route::resource('/peripherals', 'PeripheralController');
	Route::patch('/peripherals/status/{statusID}/peripheral/{peripheralID}', 'PeripheralController@updateStatus');	

	Route::get('/maintenancelog', 'MaintenanceLogController@index');
	Route::delete('/maintenancelog/status/{mlID}', 'MaintenanceLogController@setStatus');
	Route::delete('/maintenancelog/disposal/{mlID}', 'MaintenanceLogController@setDisposal');
	Route::post('/maintenancelog/remarks/{remarksID}', 'MaintenanceLogController@getRemarks');
	Route::patch('/maintenancelog/remarks/{remarksID}/update/{formRemarks}', 'MaintenanceLogController@updateRemarks');	

	Route::get('/disposalarchive', 'DisposalArchiveController@index');
	Route::delete('/disposalarchive/restore/{dpID}', 'DisposalArchiveController@restoreItem');
	Route::delete('/disposalarchive/dispose/{dpID}', 'DisposalArchiveController@disposeItem');
	Route::post('/disposalarchive/disposed/details/get/{detailsID}', 'DisposalArchiveController@getDisposedItemDetails');
	Route::post('/disposalarchive/hasdetails/{detailsID}', 'DisposalArchiveController@hasItemDetails');
	Route::patch('/disposalarchive/details/save/{detailsID}', 'DisposalArchiveController@saveItemDetails');
	Route::post('/disposalarchive/details/edit/{detailsID}', 'DisposalArchiveController@editItemDetails');
	Route::patch('/disposalarchive/details/update/{detailsID}', 'DisposalArchiveController@updateItemDetails');

	Route::get('/users/deactivated', 'UserController@showDeactivatedUsers');
	Route::patch('/users/restore/{userID}', 'UserController@restore');
	Route::delete('/users/permanentdelete/{userID}', 'UserController@permanentDelete');
	Route::resource('/users', 'UserController');
});

// Route::middleware(['owner'])->group(function(){
// 	// put route for managing administrators here
// });